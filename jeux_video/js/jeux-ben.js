

let page = document.querySelector("body");
let playground = document.querySelector("#playground");
let fond = document.querySelector('#fond');
let imgBloc = document.querySelector('#Bloc');

let player = document.createElement('img');
player.className = 'player';
player.src = "Images/theprince.png";
playground.appendChild(player);


let playerCaract = {
    x: 30,
    y: 385,
    width: 10,
    height: 35,
};
player.style.left = playerCaract.x + "px";
player.style.top = playerCaract.y + "px";


let blocPlein = document.createElement('img');
blocPlein.className = 'plein';
blocPlein.src = "Images/PrinceOfPersia.png";
playground.appendChild(blocPlein);

let blocObstacle = document.createElement('img');
blocObstacle.className = 'detour';
blocObstacle.src = "Images/PrinceOfPersia.png";
playground.appendChild(blocObstacle);

let blocObstacle2 = document.createElement('img');
blocObstacle2.className = 'detour2';
blocObstacle2.src = "Images/PrinceOfPersia.png";
playground.appendChild(blocObstacle2);

let blocObstacle3 = document.createElement('img');
blocObstacle3.className = 'detour3';
blocObstacle3.src = "Images/PrinceOfPersia.png";
playground.appendChild(blocObstacle3);

let blocObstacle4 = document.createElement('img');
blocObstacle4.className = 'detour4';
blocObstacle4.src = "Images/PrinceOfPersia.png";
playground.appendChild(blocObstacle4);

let blocObstacle5 = document.createElement('img');
blocObstacle5.className = 'detour5';
blocObstacle5.src = "Images/PrinceOfPersia.png";
playground.appendChild(blocObstacle5);

let blocObstacle6 = document.createElement('img');
blocObstacle6.className = 'detour6';
blocObstacle6.src = "Images/coin.png";
playground.appendChild(blocObstacle6);

let blocObstacle7 = document.createElement('img');
blocObstacle7.className = 'detour7';
blocObstacle7.src = "Images/PrinceOfPersia.png";
playground.appendChild(blocObstacle7);

let blocObstacle8 = document.createElement('img');
blocObstacle8.className = 'detour8';
blocObstacle8.src = "Images/PrinceOfPersia.png";
playground.appendChild(blocObstacle8);

let blocObstacle9 = document.createElement('img');
blocObstacle9.className = 'detour9';
blocObstacle9.src = "Images/PrinceOfPersia.png";
playground.appendChild(blocObstacle9);

let blocObstacle10 = document.createElement('img');
blocObstacle10.className = 'detour10';
blocObstacle10.src = "Images/PrinceOfPersia.png";
playground.appendChild(blocObstacle10);

let blocObstacle11 = document.createElement('img');
blocObstacle11.className = 'detour11';
blocObstacle11.src = "Images/coin.png";
playground.appendChild(blocObstacle11);

let blocObstacle12 = document.createElement('img');
blocObstacle12.className = 'detour12';
blocObstacle12.src = "Images/coin.png";
playground.appendChild(blocObstacle12);

let blocObstacle13 = document.createElement('img');
blocObstacle13.className = 'detour13';
blocObstacle13.src = "Images/coin.png";
playground.appendChild(blocObstacle13);

let blocObstacle14 = document.createElement('img');
blocObstacle14.className = 'detour14';
blocObstacle14.src = "Images/coin.png";
playground.appendChild(blocObstacle14);

let blocObstacle15 = document.createElement('img');
blocObstacle15.className = 'detour15';
blocObstacle15.src = "Images/coin.png";
playground.appendChild(blocObstacle15);

let bordureGauche = document.createElement('img');
bordureGauche.className = 'bordureGauche';
bordureGauche.src = "Images/PrinceOfPersia.png";
playground.appendChild(bordureGauche)

let bordureDroite = document.createElement('img');
bordureDroite.className = 'bordureDroite';
bordureDroite.src = "Images/PrinceOfPersia.png";
playground.appendChild(bordureDroite)

let blocPlein2 = document.createElement('img');
blocPlein2.className = 'plein2';
blocPlein2.src = "Images/PrinceOfPersia.png";
playground.appendChild(blocPlein2);

let tab = [blocObstacle, blocObstacle2, blocObstacle3, blocObstacle4, blocObstacle5, blocObstacle7,
           blocObstacle8, blocObstacle9, blocObstacle10, bordureDroite, blocPlein, blocPlein2, bordureGauche];

window.addEventListener('mousemove', function (event) {
    //console.log(event);
});

page.addEventListener("keydown", function (event) {
    //console.log(event.key);
    console.log(player);
    //console.log(playerCaract)

    if (event.key === 'ArrowRight') {
        if (player.offsetLeft < playground.offsetWidth - player.offsetWidth) {
            if (contact(playerCaract.x + 30, playerCaract.y, playerCaract.width, playerCaract.height) === true) {
                playerCaract.x += 30;
                player.style.left = playerCaract.x + "px";
            }
        }
        else {
            playerCaract.x = playground.offsetWidth - player.offsetWidth;
            player.style.left = playerCaract.x + "px";
        }
    }

    if (event.key === 'ArrowLeft') {
        if (player.offsetLeft > 0) {
            if (contact(playerCaract.x - 30, playerCaract.y, playerCaract.width, playerCaract.height) === true) {
                playerCaract.x -= 30;
                player.style.left = playerCaract.x + "px";
            }
        } else {
            playerCaract.x = 0;
            player.style.left = playerCaract.x + "px";
        }
    }

    if (event.key === 'ArrowUp' && player.offsetTop >= 20) {
        if (contact(playerCaract.x, playerCaract.y - 75, playerCaract.width, playerCaract.height) === true) {
            playerCaract.y -= 75;
            player.style.top = playerCaract.y + "px";
            // let count = 0;
            // let intervalle = setInterval(function () {
            //     console.log('bloup');
            //     count++;
            //     if (count >= 40) {
            //         clearInterval(intervalle)
            //     }
            //     if(contact(playerCaract.x, playerCaract.y   , playerCaract.width, playerCaract.height) === false) {
            //         playerCaract.y = player.offsetTop ;
            //         player.style.top = playerCaract.y + "px";
            //         clearInterval(intervalle);
            //     }
            //     if(count === 30){
            //         playerCaract.y += 65;   
            //         player.style.top = playerCaract.y + "px";
            //     }
            // }, 50)
        }
    }
    else if (event.key === 'ArrowDown' && player.offsetTop <= 300) {
        if (contact(playerCaract.x, playerCaract.y + 10, playerCaract.width, playerCaract.height) === true) {
            playerCaract.y += 10;
            player.style.top = playerCaract.y + "px";
        }
    }
});

setInterval(function() {
    if (contact(playerCaract.x, playerCaract.y , playerCaract.width, playerCaract.height + 10)  === true) {
        playerCaract.y += 5;
        player.style.top = playerCaract.y + "px";
    }
},50)




