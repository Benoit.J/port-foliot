class Bloc{
/**
 * @param {number} paraX 
 * @param {number} paraY 
 * @param {number} paraWidth 
 * @param {number} paraHeight 
 * @param {number} paraColor 
 * @param {number} paraBorder 
 */
    constructor(paraX,paraY,paraWidth,paraHeight,paraColor,paraBorder){
        this.x = paraX;
        this.y = paraY;
        this.width = paraWidth;
        this.height = paraHeight;
        this.color = paraColor;
        this.border = paraBorder;
    }
}